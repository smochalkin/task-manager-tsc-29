package ru.smochalkin.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String PASSWORD_ITERATION = "iteration";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "99";

    @NotNull
    public static final String PASSWORD_SECRET = "secret";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    private String getValue(@NotNull final String name, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(name)) return System.getProperty(name);
        if (System.getenv().containsKey(name)) return System.getenv(name);
        return properties.getProperty(name, defaultValue);
    }

    @NotNull
    private Integer getValueInt(@NotNull final String name, @NotNull final String defaultValue) {
        @NotNull String value;
        if (System.getenv().containsKey(name)) {
            value = System.getenv(name);
        } else if (System.getProperties().containsKey(name)) {
            value = System.getenv(name);
        } else {
            value = properties.getProperty(name, defaultValue);
        }
        return Integer.valueOf(value);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read("version");
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return Manifests.read("email");
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return Manifests.read("developer");
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getValueInt(PASSWORD_ITERATION, PASSWORD_ITERATION_DEFAULT);
    }

}

