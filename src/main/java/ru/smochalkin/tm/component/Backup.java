package ru.smochalkin.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.data.BackupLoadCommand;
import ru.smochalkin.tm.command.data.BackupSaveCommand;


public class Backup extends Thread {

    private final int interval = 10000;

    @NotNull
    public final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.parseCommand(BackupSaveCommand.NAME);
    }

    public void load() {
        bootstrap.parseCommand(BackupLoadCommand.NAME);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(interval);
        }
    }

}


