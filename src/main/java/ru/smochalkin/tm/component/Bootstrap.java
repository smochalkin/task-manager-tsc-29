package ru.smochalkin.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.smochalkin.tm.api.repository.ICommandRepository;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.constant.TerminalConst;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.system.UnknownCommandException;
import ru.smochalkin.tm.repository.CommandRepository;
import ru.smochalkin.tm.repository.ProjectRepository;
import ru.smochalkin.tm.repository.TaskRepository;
import ru.smochalkin.tm.repository.UserRepository;
import ru.smochalkin.tm.service.*;
import ru.smochalkin.tm.util.SystemUtil;
import ru.smochalkin.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    public final IUserRepository userRepository = new UserRepository();

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    public final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    public final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this);

    private void init() {
        initPID();
        initData();
        initCommands();
        backup.init();
    }

    private void initData() {
        @NotNull final String userId = userService.create("user", "1", "user@user.ru").getId();
        @NotNull final String adminId = userService.create("admin", "100", Role.ADMIN).getId();
        authService.login("admin", "100");
        projectService.create(userId, "ProjectB", "lorem ipsum");
        projectService.create(userId, "ProjectD", "lorem ipsum").setStatus(Status.IN_PROGRESS);
        projectService.create(adminId, "ProjectA", "lorem ipsum").setStatus(Status.COMPLETED);
        projectService.create(adminId, "ProjectC", "lorem ipsum").setStatus(Status.IN_PROGRESS);
        taskService.create(userId, "TaskB", "lorem ipsum");
        taskService.create(userId, "TaskA", "lorem ipsum").setStatus(Status.COMPLETED);
        taskService.create(adminId, "TaskC", "lorem ipsum").setStatus(Status.IN_PROGRESS);
        taskService.create(adminId, "TaskD", "lorem ipsum").setStatus(Status.COMPLETED);
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.smochalkin.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class).
                        stream().
                        sorted(Comparator.comparing(Class::getName)).
                        collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            registry(clazz.getConstructor().newInstance());
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(@Nullable final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        init();
        parseArgs(args);
        process();
    }

    public void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public void process() {
        logService.debug("process start...");
        @NotNull String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("Enter command: ");
            command = TerminalUtil.nextLine();
            logService.command(command);
            try {
                parseCommand(command);
                logService.info("`" + command + "` command executed");
            } catch (Exception e) {
                logService.error(e);
            }
            System.out.println();
        }
    }

    public void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException();
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    public void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @Override
    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    @NotNull
    public IPropertyService getPropertyService() {
        return propertyService;
    }

}
