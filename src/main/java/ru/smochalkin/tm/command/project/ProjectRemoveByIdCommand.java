package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        serviceLocator.getAuthService().getUserId();
        System.out.print("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectTaskService().isNotProjectId(projectId)) {
            throw new ProjectNotFoundException();
        }
        serviceLocator.getProjectTaskService().removeProjectById(projectId);
        System.out.println("[OK]");
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
