package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-start-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Start project by id.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        serviceLocator.getAuthService().getUserId();
        System.out.print("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(id);
        serviceLocator.getProjectService().updateStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
