package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-show-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        serviceLocator.getAuthService().getUserId();
        System.out.print("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().findById(id);
        showProject(project);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
