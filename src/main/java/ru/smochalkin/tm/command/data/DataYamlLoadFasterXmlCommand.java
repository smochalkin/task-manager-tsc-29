package ru.smochalkin.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-yaml-load";
    }

    @Nullable
    @Override
    public String description() {
        return "Load YAML data from file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_YAML)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

}

