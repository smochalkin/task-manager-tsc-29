package ru.smochalkin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.dto.Domain;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String FILE_BINARY = "./data.bin";

    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    protected static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";

    @NotNull
    protected static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";

    @NotNull
    protected static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";

    @NotNull
    protected static final String FILE_JAXB_JSON = "./data-jaxb.json";

    @NotNull
    protected static final String FILE_JAXB_XML = "./data-jaxb.xml";

    @NotNull
    protected static final String JAXB_JSON_PROPERTY_NAME = "eclipselink.media-type";

    @NotNull
    protected static final String JAXB_JSON_PROPERTY_VALUE = "application/json";

    @NotNull
    protected static final String FILE_BACKUP = "./backup.xml";

    @NotNull
    @SneakyThrows
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        if (serviceLocator == null) throw new EntityNotFoundException();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    @SneakyThrows
    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        if (serviceLocator == null) throw new EntityNotFoundException();
        serviceLocator.getUserService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getAuthService().logout();
    }

}

