package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-update-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        serviceLocator.getAuthService().getUserId();
        System.out.print("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(id);
        System.out.print("Enter new name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("Enter new description: ");
        @Nullable final String desc = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateById(id, name, desc);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
