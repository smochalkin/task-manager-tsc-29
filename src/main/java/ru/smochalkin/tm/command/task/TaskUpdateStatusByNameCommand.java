package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.StatusNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskUpdateStatusByNameCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-status-update-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Update task status by name.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findByName(userId, name);
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusName = TerminalUtil.nextLine();
        @NotNull final Status status = Status.getStatus(statusName);
        serviceLocator.getTaskService().updateStatusByName(userId, name, status);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
