package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowListByProjectIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-show-by-project-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Show tasks by project id.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        serviceLocator.getAuthService().getUserId();
        System.out.print("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectTaskService().isNotProjectId(projectId)) {
            throw new ProjectNotFoundException();
        }
        System.out.println("[TASK LIST BY PROJECT ID = "+ projectId + "]");
        @NotNull final List<Task> tasks = serviceLocator.getProjectTaskService().findTasksByProjectId(projectId);
        for (Task task : tasks) {
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
